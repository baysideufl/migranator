<?php

class BaseMigranator
{

	protected $targetDb;
	protected $sourceDb;
	protected $migrations;
	protected $log;
	public $invalid = array();


	/**
	 * Set the migrations to a class variable. Establish DB connections.
	 * @param array $yaml - Array of parameters parsed from the YAML config file.
	 */
	function __construct($yaml) {
		$this->migrations = isset($yaml['migrations']) ? $yaml['migrations'] : null;
		$credentials = isset($yaml['credentials']) ? $yaml['credentials'] : null;

		// establish DB connections
		if ($credentials) {
			try {
				$this->sourceDb = new PDO("mysql:host={$credentials['source']['host']};dbname={$credentials['source']['name']}", $credentials['source']['user'], $credentials['source']['password']);
				$this->targetDb = new PDO("mysql:host={$credentials['target']['host']};dbname={$credentials['target']['name']}", $credentials['target']['user'], $credentials['target']['password']);
			} catch (Exception $e) {
				echo $e->getMessage(); exit;
			}
		}

		// create/open the log file for writing
		$logFilename = __dir__ . "/log/migration_errors.txt";
		if (!file_exists(pathinfo($logFilename)['dirname'])) {
			mkdir(pathinfo($logFilename)['dirname'],0777,true);
		}
		$this->log = fopen($logFilename, "a");
	}

	function __desctruct(){
		fclose($this->log);
	}

	/**
	BEGIN LOGIC STUFF
	 */

	/**
	 * Primary method to handle all migrations.
	 * @param  array  $options 
	 *         				'max_execution_time' => int - explicitly set php script max execution time in seconds.
	 * @return echo   Echoes success or failure message to the screen.
	 */
	public function handleMigrations($options=array()){
		ini_set('max_execution_time', isset($options['max_execution_time']) ? $options['max_execution_time'] : 600);
		if ($this->migrations) {
			foreach ($this->migrations as $key => $migration) {
				echo "Migrating {$key}\n";
				$method = ucfirst(strtolower($migration['method']));
				$method = "handle" . $method . "Migration";
				$this->$method($migration);
				$this->writeErrorLog();
			}
			echo "Migration complete!\n";
		}else{
			die("No migrations defined!");			
		}
	}

	/**
	 * Write any errors to 'log/migration_errors'
	 * @return no return
	 */
	protected function writeErrorLog(){
		$date = new DateTime('now');
		$msg = "";
		foreach ($this->invalid as $error) {
			$msg .= $date->format('Y-m-d H:i:s') . " - {$error['message']}\n";
		}
		fwrite($this->log, $msg);
		$this->invalid = array();
	}

	/**
	 * Primary method to handle built-in/simple migrations.
	 * @param  array $config - array of migration config from a specific migration
	 * @return no return
	 */
	protected function handleSimpleMigration($config){
		$query = $this->sourceDb->query("SELECT * FROM {$config['from']}");
		$query->setFetchMode(PDO::FETCH_ASSOC);

		// get the rows of data
		while($row = $query->fetch()) {
			$model = array();
			$columns = array();

			// foreach field within a row
			foreach ($config['fields'] as $field) {
				$valueType = $field['type'] . 'Value';
				$model[":".$field['to']] = $this->$valueType($field, $row);
				$columns[] = $field['to'];

				// move file if file field
				if (isset($field['file'])) {
					$from = __dir__ . $field['file']['from_path'] . $row[$field['to']];
					$to = __dir__ . $field['file']['to_path'] . $row[$field['to']];
					$this->smartCopy($from, $to);
				}

				// check for required field
				if (isset($field['required']) && $field['required']) {
					if ($model[":".$field['to']] === null) {
						$source_row = print_r($row, true);
						$target_row = print_r($model, true);
						$this->invalid[] = array('message' => "Required field missing from table `{$config['from']}`: \nSource: \n{$source_row}Target:\n{$target_row}");
						continue 2;
					}
				}
			} // end field loop

			// insert the thing
			$insertSql = "INSERT INTO `{$config['to']}` (" . implode(", ", $columns) .") VALUES (" . implode(", ", array_keys($model)) .")";
			$insert = $this->targetDb->prepare($insertSql);
			$result = $insert->execute($model);

			// add to error log if it failed
			if (!$result) {
				$source_row = print_r($row, true);
				$error_message = print_r($insert->errorInfo(), true);
				$target_row = print_r($model, true);
				$this->invalid[] = array('message' => "PDO error occured while inserting row into `{$config['from']}`: \nSource: \n{$source_row}Target: \n{$target_row} \nPDO error \n{$error_message}");
			}

			// move any bulk files
			if (isset($config['files'])) {
				foreach ($config['files']['fields'] as $f) {
					if ($f) {
						$from = __dir__ . $config['files']['from_path'] . $row[$f];
						$to = __dir__ . $config['files']['to_path'] . $row[$f];
						$this->smartCopy($from, $to);
					}
				}
			}

		} // end rows loop
	}

	/**
	VALUE HELPER FUNCTIONS
	 */

	/**
	 * Map Column 1-1: This will simply migrate a column from the source DB to a column in the target DB.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function mapsToValue($field, $row){
		return $row[$field['from']];
	}

	/**
	 * Set Field to Value: Simply sets a fieds to a given value.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function setToValue($field, $row){
		return $field['value'];
	}

	/**
	 * Hash: Generates a hash from concatenated fields. Uses bcrypt hashing algorithm.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function hashValue($field, $row){
		$string = "";
		foreach ($field['from'] as $f) {
			$string .= $row[$f];
		}
		return password_hash($string, PASSWORD_BCRYPT);
	}

	/**
	 * Concatenate Fields: Migrates a string of specified concatenated fields.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function concatValue($field, $row){
		$values = array();
		foreach ($field['from'] as $f) {
			$values[] = $row[$f];
		}

		return implode($field['glue'], $values);
	}

	/**
	 * Map Column 1-1 and Make Title Case: Same as `mapsTo` except it also forces title case.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function mapsToMakeTitleValue($field, $row){
		return ucwords(str_replace("_", " ", $row[$field['from']]));
	}

	/**
	 * Enumeration (first value is default): Will only allow migration of given values. Defaults to first value if no match.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function enumValue($field, $row){
		foreach ($field['values'] as $value) {
			if ($row[$field['from']] == $value) {
				return $row[$field['from']];
			}
		}
		return $field['values'][0];
	}

	/**
	 * Find Record and Map Field: This will perform a query on the `target_table` where the `find_target_table` matches `match_source_field`. It will then get the `map_target_Field` and set it to the `to` field.
	 * @param  array $field - Array of config for a particular DB column
	 * @param  array $row - Array of data from a particular row
	 * @return string/integer
	 */
	protected function findAndMapValue($field, $row){
		$sql = "SELECT {$field['map_target_field']} FROM `{$field['target_table']}` WHERE `{$field['find_target_field']}` = :field LIMIT 1";
		$query = $this->targetDb->prepare($sql);
		$query->execute(array(':field' => $row[$field['match_source_field']] ));
		$result = $query->fetch();

		if ($result) {
			return $result[$field['map_target_field']];
		}
	}

	/**
	HELPER FUNCTIONS
	 */

	/**
	 * Will copy file and create directories as necessary
	 * @param  string $from - filepath of the source file to be copied.
	 * @param  string $to - filepath of the target where the file will be copied.
	 * @return no return
	 */
	protected function smartCopy($from, $to){
		if (!file_exists($from)) {
			$this->invalid[] = array('message' => "Unable to locate file: $from");
			return false;
		}

		if (is_dir($from)) {
			return false;
		}

		if (!file_exists(pathinfo($to)['dirname'])) {
			mkdir(pathinfo($to)['dirname'], 0777, true);
		}

		copy($from, $to);
	}

}