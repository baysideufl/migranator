#!/usr/bin/php
<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require('Migranator.class.php');

// if yaml php extention is install use that. otherwise use symfony parser.
if (function_exists('yaml_parse_file')) {
	$migranator = new Migranator(yaml_parse_file(__dir__ . "/config.yml"));
}else{
	require(__dir__ . '/yaml/Yaml.php');
	$migranator = new Migranator(Yaml::parse(file_get_contents(__dir__ . "/config.yml")));
}

// start the migrations
$migranator->handleMigrations();
